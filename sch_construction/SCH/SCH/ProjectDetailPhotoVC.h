//
//  ProjectDetailPhotoVC.h
//  SCH
//
//  Created by nayminsoe on 11/24/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectDetailPhotoVC : UIViewController


- (void)displayProjectDetail:(id)data;
@end
