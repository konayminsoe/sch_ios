//
//  LoginViewController.m
//  SCH
//
//  Created by nayminsoe on 11/24/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
@property (nonatomic, weak) IBOutlet UITextField *txtfUserName;
@property (nonatomic, weak) IBOutlet UITextField *txtfPassword;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)btnLogin_TouchUpInside:(id)sender {
    [self login];
}

- (void)login {
    //TODO
}
@end
