//
//  BarGraphClass.m
//  BarGraph
//
//  Created by  NMS on 16/1/15.
//  Copyright (c) 2015  NMS. All rights reserved.
//

#import "BarGraphClass.h"

@interface BarGraphClass() {
    CPTBarPlot *barPlot;
}
@end
@implementation BarGraphClass

// Method to create this object and attach it to it's hosting view.
-(id)initWithHostingView:(CPTGraphHostingView *)hostingView andData:(NSMutableArray *)data1 andData:(NSMutableArray *)data2 andData:(NSMutableArray *)data3
{
    self = [super init];
    
    if ( self != nil ) {
        self.hostingView = hostingView;
        self.graphData1 = data1;
        self.graphData2 = data2;
        self.graphData3 = data3;
        self.graph = nil;
    }
    return self;
}

- (void)refreshData:(NSMutableArray *)data1 :(NSMutableArray *)data2 :(NSMutableArray *)data3 {
    self.graphData1 = data1;
    self.graphData2 = data2;
    self.graphData3 = data3;
    
    NSLog(@"d1 - %@",data1);
    NSLog(@"d2 - %@",data2);
    NSLog(@"d3 - %@",data3);

}

// Specific code that creates the scatter plot.
-(void)initialisePlot:(NSArray *)xAxisLabels withyAxis:(NSInteger)y_maxvalue
{
    // Start with some simple sanity checks before we kick off
    if ( (self.hostingView == nil) || (self.graphData1 == nil) || (self.graphData2 == nil) || (self.graphData3 == nil) ) {
        NSLog(@"SimpleBarGraph: Cannot initialise plot without hosting view or data.");
        return;
    }
    
    if ( self.graph != nil ) {
        
        NSLog(@"layers = %lu", [self.graph allPlots].count);
        for (CPTBarPlot *plot in [self.graph allPlots]) {
            [plot removeFromSuperlayer];
            NSLog(@"SimpleBarGraph: Graph object already exists.");

        }
        //return;
    }
    // Create a graph object which we will use to host just one scatter plot.
    CGRect frame = [self.hostingView bounds];
    self.graph = [[CPTXYGraph alloc] initWithFrame:frame];
    
    // Tie the graph we've created with the hosting view.
    self.hostingView.hostedGraph = self.graph;
    
    // If you want to use one of the default themes - apply that here.
    //[self.graph applyTheme:[CPTTheme themeNamed:kCPTDarkGradientTheme]];
    
    // Border
	self.graph.plotAreaFrame.borderLineStyle = nil;
	self.graph.plotAreaFrame.cornerRadius	   = 0.0f;
    
	// Paddings
	self.graph.paddingLeft   = 0.0f;
	self.graph.paddingRight  = 0.0f;
	self.graph.paddingTop	 = 0.0f;
	self.graph.paddingBottom = 0.0f;
    
	self.graph.plotAreaFrame.paddingLeft	 = 45.0;
	self.graph.plotAreaFrame.paddingTop	 = 20.0;
	self.graph.plotAreaFrame.paddingRight	 = 20.0;
	self.graph.plotAreaFrame.paddingBottom = 80.0;
    
    // Graph title
	self.graph.title = @"";
	CPTMutableTextStyle *textStyle = [CPTTextStyle textStyle];
	textStyle.color					  = [CPTColor redColor];
	textStyle.fontSize				  = 14.0f;
	textStyle.textAlignment			  = CPTTextAlignmentCenter;
	self.graph.titleTextStyle			  = textStyle;
	self.graph.titleDisplacement		  = CGPointMake(0.0f, 0.0f);
	self.graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    
    // Add plot space for horizontal bar charts
	CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)self.graph.defaultPlotSpace;
	plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(y_maxvalue)];
    
    float width = self.graphData1.count * 2.0;
	plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(width)];
    
    //axis line style
    CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
    axisLineStyle.lineColor = [CPTColor grayColor];
    axisLineStyle.lineWidth = 1.0f;
    
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *)self.graph.axisSet;
	CPTXYAxis *x		  = axisSet.xAxis;
	x.axisLineStyle				  = axisLineStyle;
	x.majorTickLineStyle		  = nil;
	x.minorTickLineStyle		  = nil;
	x.majorIntervalLength		  = CPTDecimalFromString(@"3");
	x.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
	x.title						  = @"";
	x.titleLocation				  = CPTDecimalFromFloat(4.5f);
	x.titleOffset				  = 55.0f;
    
    /****
     X Axis Custom Labels
     ****/
	// Define some custom labels for the data elements
	x.labelRotation	 = M_PI / 4;
	x.labelingPolicy = CPTAxisLabelingPolicyNone;
    NSArray *customTickLocations = nil;
    NSMutableArray *tikers = [NSMutableArray array];
    //NSArray *xAxisLabels		 = [NSArray arrayWithObjects:@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"Jun", @"Jul", @"Aug", @"Sep", @"Oct", @"Nov", @"Dec", nil];
    
    for (int i=0; i<xAxisLabels.count; i++) {
        [tikers addObject:[NSDecimalNumber numberWithInt:1 + (i * 2)]];
    }
    customTickLocations = tikers;
    
	NSUInteger labelLocation	 = 0;
	NSMutableArray *customLabels = [NSMutableArray arrayWithCapacity:[xAxisLabels count]];
	
    for ( NSNumber *tickLocation in customTickLocations ) {
		CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:[[xAxisLabels objectAtIndex:labelLocation++] uppercaseString] textStyle:textStyle];
        newLabel.tickLocation = [tickLocation decimalValue];
		newLabel.offset		  = x.labelOffset + x.majorTickLength;
		//newLabel.rotation	  = M_PI / 4;
		[customLabels addObject:newLabel];
	}
	x.axisLabels = [NSSet setWithArray:customLabels];
    
    /****
     Y Axis Custom Labels
     ****/
	CPTXYAxis *y = axisSet.yAxis;
	y.axisLineStyle				  = axisLineStyle;
	y.majorTickLineStyle		  = nil;
	y.minorTickLineStyle		  = nil;
    y.majorIntervalLength		  = CPTDecimalFromString(@"10");
	y.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
	y.title						  = @"";
	y.titleOffset				  = 45.0f;
    
    // Define some custom labels for the data elements
    //y.labelRotation	 = M_PI / 4;
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    NSArray *yCustomTickLocations = nil;
    NSMutableArray *ytikers = [NSMutableArray array];
    //NSArray *yAxisLabels		 = [NSArray arrayWithObjects:@"0", @"1K", @"2K", @"3K", @"4K", @"5K", @"6K", @"7K", @"8K", @"9K", nil];
    
    int labelInterval = 5;
    
    for (int i=0; i<labelInterval; i++) {
        [ytikers addObject:[NSDecimalNumber numberWithInt: (int)(i * (y_maxvalue/labelInterval))]];
    }
    yCustomTickLocations = ytikers;
    
    NSUInteger yLabelLocation	 = 0;
    NSMutableArray *yCustomLabels = [NSMutableArray arrayWithCapacity:labelInterval];
    
    for ( NSNumber *tickLocation in yCustomTickLocations ) {
        
        if (yLabelLocation == 0) {
            yLabelLocation++;
        }else {
            CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:[NSString stringWithFormat:@"%luK",(y_maxvalue/labelInterval) * yLabelLocation++] textStyle:textStyle];
            newLabel.tickLocation = [tickLocation decimalValue];
            newLabel.offset		  = y.labelOffset + y.majorTickLength;
            //newLabel.rotation	  = M_PI / 4;
            [yCustomLabels addObject:newLabel];
        }
    }
    y.axisLabels = [NSSet setWithArray:yCustomLabels];
    
	// First bar plot
	CPTBarPlot *barPlot1					= [CPTBarPlot tubularBarPlotWithColor:[CPTColor yellowColor] horizontalBars:NO];
	barPlot1.dataSource		= self;
	barPlot1.baseValue		= CPTDecimalFromString(@"0");
	barPlot1.barCornerRadius = 0.0f;
	barPlot1.identifier		= @"Bar Plot1";
    
    // second bar plot
    CPTBarPlot *barPlot2					= [CPTBarPlot tubularBarPlotWithColor:[CPTColor greenColor] horizontalBars:NO];
    barPlot2.dataSource		= self;
    barPlot2.baseValue		= CPTDecimalFromString(@"0");
    barPlot2.barCornerRadius = 0.0f;
    barPlot2.identifier		= @"Bar Plot2";
    
    // third bar plot
    CPTBarPlot *barPlot3					= [CPTBarPlot tubularBarPlotWithColor:[CPTColor redColor] horizontalBars:NO];
    barPlot3.dataSource		= self;
    barPlot3.baseValue		= CPTDecimalFromString(@"0");
    barPlot3.barCornerRadius = 0.0f;
    barPlot3.identifier		= @"Bar Plot3";
    
    // 1 - Add plots to graph
    CGFloat const CPDBarWidth = 0.5f;
    CGFloat const CPDBarInitialX = 0.5;

    // 2 - Set up line style
    CPTMutableLineStyle *barLineStyle = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineColor = [CPTColor lightGrayColor];
    barLineStyle.lineWidth = 0;

    CGFloat barX = CPDBarInitialX;
    
    NSArray *plots = [NSArray arrayWithObjects:barPlot1, barPlot2, barPlot3, nil];
    for (CPTBarPlot *plot in plots) {
        plot.barWidth = CPTDecimalFromDouble(CPDBarWidth);
        plot.barOffset = CPTDecimalFromDouble(barX);
        plot.lineStyle = barLineStyle;
        
        [self.graph addPlot:plot toPlotSpace:plotSpace];
        barX += CPDBarWidth+0.1;
    }
}

#pragma mark -
#pragma mark Plot Data Source Methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
 
    if ([plot.identifier isEqual:@"Bar Plot1"]) {
        return [self.graphData1 count];
    } else if ([plot.identifier isEqual:@"Bar Plot2"]) {
        return [self.graphData2 count];
    }else {
        return [self.graphData3 count];
    }
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSValue *value = nil;
    
    if ([plot.identifier isEqual:@"Bar Plot1"]) {
        value = [self.graphData1 objectAtIndex:index];
    }else if ([plot.identifier isEqual:@"Bar Plot2"]) {
        value = [self.graphData2 objectAtIndex:index];
    }else {
        value = [self.graphData3 objectAtIndex:index];
    }
    
    CGPoint point = [value CGPointValue];
    if([plot isKindOfClass:[CPTBarPlot class]])
    {
        if(fieldEnum == CPTBarPlotFieldBarLocation)
        {
            return [NSNumber numberWithFloat:point.x];
        }
        else if(fieldEnum == CPTBarPlotFieldBarTip)
        {
            return [NSNumber numberWithFloat:point.y];
        }
    }
    
    return [NSNumber numberWithFloat:0];
}

-(CPTFill *)barFillForBarPlot:(CPTBarPlot *)_barPlot recordIndex:(NSUInteger)index {
    
    if ([_barPlot.identifier isEqual:@"Bar Plot1"]) {
        return [CPTFill fillWithColor:[CPTColor yellowColor]];
    
    }else if ([_barPlot.identifier isEqual:@"Bar Plot2"]) {

        return [CPTFill fillWithColor:[CPTColor greenColor]];
    }else {
        
        return [CPTFill fillWithColor:[CPTColor whiteColor]];
    }
}

-(CPTLineStyle *)barLineStyleForBarPlot:(CPTBarPlot *)barPlot recordIndex:(NSUInteger)index {
    
    //axis line style
    CPTMutableLineStyle *lineStyle = [CPTMutableLineStyle lineStyle];
    lineStyle.lineColor = [CPTColor grayColor];
    lineStyle.lineWidth = 1;
    
    return lineStyle;

}

@end
