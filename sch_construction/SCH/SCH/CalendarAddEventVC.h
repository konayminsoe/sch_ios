//
//  CalendarAddEventVC.h
//  SCH
//
//  Created by nayminsoe on 1/12/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CalendarAddEventDelegate <NSObject>
- (void)didAddEvent:(id)newEvent;
@end
@interface CalendarAddEventVC : UIViewController

@property (nonatomic, weak) id <CalendarAddEventDelegate> delegate;
@end
