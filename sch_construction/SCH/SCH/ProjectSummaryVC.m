//
//  ProjectSummaryVC.m
//  SCH
//
//  Created by nayminsoe on 11/24/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import "ProjectSummaryVC.h"
#import "ProjectCell.h"
#import "ProjectDetailPhotoVC.h"

@interface ProjectSummaryVC ()<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UITextFieldDelegate> {
    NSArray *filters;
    NSMutableArray *projects;
}

@property (strong, nonatomic) ProjectDetailPhotoVC *detailPhotoVC;
@property (strong, nonatomic) IBOutlet UITextField *txtfSearch;
@property (strong, nonatomic) IBOutlet UIButton *btnFilter;

@end

@implementation ProjectSummaryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    filters = @[@"PROJECT NAME", @"PROJECT PROGRESS", @"PROJECT COST", @"TEST DATE"];
    
    projects = [NSMutableArray array];
    
    NSDictionary *dummyd1 = [NSDictionary dictionaryWithObjectsAndKeys:
                             @"Eu Habitat", @"projectname",
                             @"2015-01-18T11:24:00Z", @"testdate",
                             @"-$50,000", @"price", @"75", @"percent",
                             @"blk 948, Yishun St 71, #02-333, Singapore 943989", @"address", nil];
    
    NSDictionary *dummyd2 = [NSDictionary dictionaryWithObjectsAndKeys:
                             @"Eu Habitat", @"projectname",
                             @"2015-01-18T11:24:00Z", @"testdate",
                             @"$70,000", @"price", @"100", @"percent", @"blk 948, Yishun St 71, #02-333, Singapore 943989", @"address", nil];
    
    
    NSDictionary *dummyd3 = [NSDictionary dictionaryWithObjectsAndKeys:
                             @"Eu Habitat", @"projectname",
                             @"2015-01-18T11:24:00Z", @"testdate",
                             @"-$20,000", @"price", @"68", @"percent", @"blk 948, Yishun St 71, #02-333, Singapore 943989", @"address", nil];
    
    
    NSDictionary *dummyd4 = [NSDictionary dictionaryWithObjectsAndKeys:
                             @"Eu Habitat", @"projectname",
                             @"2015-01-18T11:24:00Z", @"testdate",
                             @"$90,000", @"price", @"35", @"percent", @"blk 948, Yishun St 71, #02-333, Singapore 943989", @"address", nil];
    
    
    NSDictionary *dummyd5 = [NSDictionary dictionaryWithObjectsAndKeys:
                             @"Eu Habitat", @"projectname",
                             @"2015-01-18T11:24:00Z", @"testdate",
                             @"$10,000", @"price", @"20", @"percent", @"blk 948, Yishun St 71, #02-333, Singapore 943989", @"address", nil];
    
    [projects addObjectsFromArray:@[dummyd1, dummyd2, dummyd3, dummyd4, dummyd5]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return projects.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 86;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProjectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProjectCell"];
    [tableView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor: [UIColor clearColor]];
    
    [cell bind:projects[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!self.detailPhotoVC) {
        self.detailPhotoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailPhoto"];
    }
    
    [self.detailPhotoVC displayProjectDetail:projects[indexPath.row]];
    [self.navigationController pushViewController:self.detailPhotoVC animated:YES];
    
}

- (IBAction)btnFilter_TouchUpInside:(id)sender {
    
 
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Project Name", @"Budget", @"Start Date", @"End Date", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSLog(@"%lu", buttonIndex);
    
    if (buttonIndex == filters.count) {
        return;
    }
    [_btnFilter setTitle:filters[buttonIndex] forState:UIControlStateNormal];
    
}

#pragma mark - TextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_txtfSearch resignFirstResponder];
    return YES;
}
@end
