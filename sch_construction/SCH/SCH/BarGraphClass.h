//
//  BarGraphClass.h
//  BarGraph
//
//  Created by  NMS on 16/1/15.
//  Copyright (c) 2015  NMS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CorePlot-CocoaTouch.h"

@interface BarGraphClass : NSObject<CPTBarPlotDataSource>
{
    CPTGraphHostingView *_hostingView;
    CPTXYGraph *_graph;
}
@property (nonatomic, retain) CPTGraphHostingView *hostingView;
@property (nonatomic, retain) CPTXYGraph *graph;
@property (nonatomic, retain) NSMutableArray *graphData1, *graphData2, *graphData3;

// Method to create this object and attach it to it's hosting view.
-(id)initWithHostingView:(CPTGraphHostingView *)hostingView
                 andData:(NSMutableArray *)data1
                 andData:(NSMutableArray *)data2
                 andData:(NSMutableArray *)data3;

// Specific code that creates the scatter plot.
//-(void)initialisePlot:(NSArray *)xAxisLabels;
-(void)initialisePlot:(NSArray *)xAxisLabels withyAxis:(NSInteger)maxY;

- (void)refreshData:(NSMutableArray *)data1
                   :(NSMutableArray *)data2
                   :(NSMutableArray *)data3;
@end
