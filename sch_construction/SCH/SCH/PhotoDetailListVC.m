//
//  PhotoDetailListVC.m
//  SCH
//
//  Created by nayminsoe on 1/18/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import "PhotoDetailListVC.h"
#import "Global.h"
#import "PhotoRecordCell.h"
#import "ProjectDetailPhotoPreviewVC.h"
#import "ProjectDetailGraphVC.h"
#import "ProjectDetailCheckVC.h"
#import "UploadPhotoVC.h"

@interface PhotoDetailListVC ()<UINavigationControllerDelegate>
{
    id photoRecords;
}
@property (weak, nonatomic) ProjectDetailPhotoPreviewVC *photoPreviewVC;
@property (weak, nonatomic) IBOutlet UITableView *tblPhotoList;


@end

@implementation PhotoDetailListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.delegate = self;
    [self applyData];
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
     UIViewController *vc = [navigationController.viewControllers lastObject];
    if ([vc isKindOfClass:[ProjectDetailGraphVC class]]
        ||
        [vc isKindOfClass:[ProjectDetailCheckVC class]]
        || [vc isKindOfClass:[UploadPhotoVC class]]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ENABLE_TABLEVIEW" object:[NSNumber numberWithBool:NO]];
        
        //hide/show camera controls
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HIDE_TABBAR" object:[NSNumber numberWithBool:YES]];


    }else {
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ENABLE_TABLEVIEW" object:[NSNumber numberWithBool:YES]];
        
        //hide/show camera controls
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HIDE_TABBAR" object:[NSNumber numberWithBool:NO]];

    }
   
    NSLog(@">>> %lu",
          navigationController.viewControllers.count);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)applyData {
    //TODO
    NSDictionary *d1 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-01-18T11:24:00Z", @"date",
                        @"1000", @"hours",
                        @"Fire panel almost done. Will need more PVC pipes", @"desc", nil];
    NSDictionary *d2 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-01-18T11:24:00Z", @"date",
                        @"7000", @"hours",
                        @"Fire alarm almost done. Will need more PVC pipes", @"desc", nil];
    
    NSDictionary *d3 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-01-18T11:24:00Z", @"date",
                        @"2000", @"hours",
                        @"Fire panel almost done. Will need more PVC pipes", @"desc", nil];
    NSDictionary *d4 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-02-18T11:24:00Z", @"date",
                        @"4000", @"hours",
                        @"Fire panel almost done. Will need more PVC pipes", @"desc", nil];
    NSDictionary *d5 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-02-18T11:24:00Z", @"date",
                        @"500", @"hours",
                        @"Alarm almost done. Will need more PVC pipes", @"desc", nil];
    
    photoRecords = [NSMutableArray arrayWithObjects:@[d1, d2, d3], @[d4, d5], nil];
    [self.tblPhotoList reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    id info = photoRecords[section];
    return [info count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 40.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    id data = photoRecords[section];
    id info = [data firstObject];
    
    NSDate *date = [GlobalShareInstance dateFormat:info[@"date"]];
    NSString *header = [Global convertDateToString:date wantedFormat:@"MMMM yyyy"];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(15, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    view.backgroundColor = [UIColor clearColor];
    UILabel *lbl = [[UILabel alloc] initWithFrame:view.frame];
    lbl.font = [UIFont fontWithName:@"FuturaStd-Bold" size:16.0];
    lbl.text = header;
    lbl.textColor = [UIColor whiteColor];
    [view addSubview:lbl];
    return view;
}
 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [photoRecords count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 52;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

        PhotoRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PhotoRecordCell"];
        [tableView setBackgroundColor:[UIColor clearColor]];
        [cell setBackgroundColor: [UIColor clearColor]];
        
        id group = photoRecords[indexPath.section];
        [cell bind:group[indexPath.row]];
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!self.photoPreviewVC) {
        self.photoPreviewVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PreviewPhoto"];
    }
    
    [self.navigationController pushViewController:self.photoPreviewVC animated:YES];
    /*
     id group = photoRecords[indexPath.section];
     id info = group[indexPath.row];
     id photoId = info[@"id"];
     
     if (!imgPreview) {
     imgPreview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
     imgPreview.backgroundColor = [UIColor blueColor];
     [imgPreview setUserInteractionEnabled:YES];
     [imgPreview addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapPreviewPhoto:)]];
     }
     [self.tblPhotoList addSubview:imgPreview];
     imgPreview.frame = CGRectMake(0, 0, self.tblPhotoList.frame.size.width, self.tblPhotoList.frame.size.height);
     self.tblPhotoList.scrollEnabled = NO;
     */
    
}

- (void)didTapPreviewPhoto:(UITapGestureRecognizer *)gest {
    [UIView animateWithDuration:0.3f animations:^{
        [[gest view] setAlpha:0.0];
    } completion:^(BOOL finished) {
        [[gest view] removeFromSuperview];
        [[gest view] setAlpha:1.0];
        self.tblPhotoList.scrollEnabled = YES;
    }];
}
@end
