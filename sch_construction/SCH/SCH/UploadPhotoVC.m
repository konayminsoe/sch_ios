//
//  UploadPhotoVC.m
//  SCH
//
//  Created by nayminsoe on 1/22/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import "UploadPhotoVC.h"
#import "Global.h"

@interface UploadPhotoVC ()
@property (nonatomic, weak) IBOutlet UIImageView *imgView;
@property (nonatomic, weak) UITextField *activeTextField;
@property (nonatomic, weak) IBOutlet UITextField *txtfHours;
@property (nonatomic, weak) IBOutlet UITextField *txtfDesc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vertConstraint;

@end

@implementation UploadPhotoVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.imgView.image = self.image;
    if ([UIScreen mainScreen].bounds.size.height == 568) {
        self.vertConstraint.constant = 290;
    }else {
        self.vertConstraint.constant = 388;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btnCancel_TouchUpInside:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSave_TouchUpInside:(id)sender {
    
    if ([GlobalShareInstance checkEmptyEntry:self.txtfHours.text] || [GlobalShareInstance checkEmptyEntry:self.txtfDesc.text]) {
        
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Enter information of the photo!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        
        return;
    }else {
        
        //TODO
        
        //GlobalShareInstance.selectedProject (project name or id here)
        //post photo and text to server
        
        [self.navigationController popViewControllerAnimated:YES];
    }
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    _txtfHours.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Hours" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    _txtfDesc.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Photo Description" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // Step 3: Scroll the target text field into view.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    //if (!CGRectContainsPoint(aRect, self.activeTextField.frame.origin) ) {
        self.view.window.frame = CGRectMake(0, -140, self.view.frame.size.width, self.view.frame.size.height);
    //}
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
}

- (void) keyboardWillHide:(NSNotification *)notification {
    self.view.window.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
