//
//  ProjectDetailPhotoVC.m
//  SCH
//
//  Created by nayminsoe on 11/24/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import "ProjectDetailPhotoVC.h"
#import "ProjectDetailGraphVC.h"
#import "ProjectCell.h"
#import "PhotoRecordCell.h"
#import "Global.h"
#import "PhotoDetailListVC.h"

@interface ProjectDetailPhotoVC ()< UITableViewDataSource> {
    id projectDetail;
    id photoRecords;
    UIImageView *imgPreview;
}
@property(nonatomic, weak) IBOutlet UITableView *tblProjectDetail;
@property (weak, nonatomic) IBOutlet UITableView *tblPhotoList;
@property (weak, nonatomic) PhotoDetailListVC *photoDetailsVC;
@property (nonatomic, strong) ProjectDetailGraphVC *graphVC;
@property (strong, nonatomic) UINavigationController *navVC;

@end

@implementation ProjectDetailPhotoVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endAbleUserTouch:) name:@"ENABLE_TABLEVIEW" object:nil];
    [self applyData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[self.navVC popToRootViewControllerAnimated:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SHOW_HIDE_TABBAR" object:[NSNumber numberWithBool:YES]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)endAbleUserTouch:(NSNotification *)notif {
    [self.tblProjectDetail setUserInteractionEnabled:[notif.object boolValue]];
}

- (void)applyData {
    //TODO
    NSDictionary *d1 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-01-18T11:24:00Z", @"date",
                        @"1000", @"hours",
                        @"Fire panel almost done. Will need more PVC pipes", @"desc", nil];
    NSDictionary *d2 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-01-18T11:24:00Z", @"date",
                        @"7000", @"hours",
                        @"Fire alarm almost done. Will need more PVC pipes", @"desc", nil];
    
    NSDictionary *d3 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-01-18T11:24:00Z", @"date",
                        @"2000", @"hours",
                        @"Fire panel almost done. Will need more PVC pipes", @"desc", nil];
    NSDictionary *d4 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-02-18T11:24:00Z", @"date",
                        @"4000", @"hours",
                        @"Fire panel almost done. Will need more PVC pipes", @"desc", nil];
    NSDictionary *d5 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-02-18T11:24:00Z", @"date",
                        @"500", @"hours",
                        @"Alarm almost done. Will need more PVC pipes", @"desc", nil];
    
    photoRecords = [NSMutableArray arrayWithObjects:@[d1, d2, d3], @[d4, d5], nil];
    
    //add tableview
    self.photoDetailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoDetailList"];
    self.navVC = [[UINavigationController alloc] initWithRootViewController:self.photoDetailsVC];
    self.navVC.toolbarHidden = YES;
    self.navVC.navigationBar.hidden = YES;
    self.navVC.view.frame = CGRectMake(0, 86, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [self.view addSubview:self.navVC.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)displayProjectDetail:(id)data {
    projectDetail = data;
    //record for further usage
    GlobalShareInstance.selectedProject = data;
    [self.tblProjectDetail reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.tblProjectDetail) {
        return 1;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (tableView == self.tblProjectDetail) {
        return nil;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (tableView == self.tblProjectDetail) {
        return 1;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProjectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell"];
    [tableView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor: [UIColor clearColor]];
    [cell bind:projectDetail];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tblProjectDetail setUserInteractionEnabled:NO];
    [self didTap];
    
}

- (void)didTap {
    self.graphVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailGraph"];
    [self.navVC pushViewController:self.graphVC animated:YES];
}

@end
