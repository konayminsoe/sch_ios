//
//  ProjectDetailGraphVC.m
//  SCH
//
//  Created by nayminsoe on 11/24/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import "ProjectDetailGraphVC.h"

@interface ProjectDetailGraphVC ()
{
    NSMutableArray *data1;
    NSMutableArray *data2;
    NSMutableArray *data3;
    NSMutableArray *org_xAxis, *dyn_xAxis;
    NSMutableArray *org_yAxis, *dyn_yAxis;
}
@end

@implementation ProjectDetailGraphVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    data1 = [NSMutableArray array];
    data2 = [NSMutableArray array];
    data3 = [NSMutableArray array];
    
    dyn_xAxis = [NSMutableArray array];
    org_xAxis = [NSMutableArray arrayWithObjects:@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"Jun", @"Jul", @"Aug", @"Sep", @"Oct", @"Nov", @"Dec", nil];
    
    
    dyn_yAxis = [NSMutableArray array];
    org_yAxis = [NSMutableArray arrayWithArray:@[@1, @2, @9, @4, @5, @6, @7, @8, @3]];
    
    BarGraphClass *plot = [[BarGraphClass alloc] initWithHostingView:_graphHostingView
                                                             andData:data1 andData:data2 andData:data3];
    self.BarPlot = plot;
    [self plot];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self plot];
}

- (void)plot {
    
    [data1 removeAllObjects];
    [data2 removeAllObjects];
    [data3 removeAllObjects];
    [dyn_xAxis removeAllObjects];
    [dyn_yAxis removeAllObjects];
    
    int idxY = 4 + arc4random() % 5;
    int idx = arc4random() % 13;
    
    for (int i=0; i<idxY; i++) {
        [dyn_yAxis addObject:org_yAxis[i]];
    }
    NSInteger maxV = [self maximumValueOfyAxis:dyn_yAxis];
    
    for (int i=0; i<idx; i++) {
        [dyn_xAxis addObject:org_xAxis[i]];
        
        int x1 = (arc4random() % maxV*100);
        int x2 = (arc4random() % maxV*100);
        int x3 = (arc4random() % maxV*100);
        
        [data1 addObject:[NSValue valueWithCGPoint:CGPointMake(i*2, x1 == 0 ? 20 : x1 )]];
        [data2 addObject:[NSValue valueWithCGPoint:CGPointMake(i*2, x2 == 0 ? 40 : x2 )]];
        [data3 addObject:[NSValue valueWithCGPoint:CGPointMake(i*2, x3 == 0 ? 30 : x3 )]];
    }
    
    [self.BarPlot refreshData:data1 :data2 :data3];
    [self.BarPlot initialisePlot:dyn_xAxis withyAxis:(maxV * 100)];
}

- (NSInteger)maximumValueOfyAxis:(NSArray *)yAxisData {
    
    NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    yAxisData = [yAxisData sortedArrayUsingDescriptors:@[sd]];
    
    NSInteger max =[[yAxisData lastObject] integerValue];
    return max;
    
}




@end
