//
//  DashboardViewController.m
//  SCH
//
//  Created by nayminsoe on 11/24/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import "DashboardViewController.h"
#import "ProjectSummaryVC.h"
#import "CalendarVC.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UploadPhotoVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "Global.h"

@interface DashboardViewController ()<UIGestureRecognizerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UINavigationController *navProjectSummary;
@property (strong, nonatomic) UITabBarController *tabbarVC;
@property (weak, nonatomic) IBOutlet UIButton *btnProjSum, *btnCalendar;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewHighlightProj;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewHighlightCalc;
@property (weak, nonatomic) IBOutlet UIView *viewTabBar;
@property (nonatomic, strong) NSArray *assets;
@property (nonatomic, strong) UploadPhotoVC *photoVC;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBg;
@property (weak, nonatomic) IBOutlet UIView *viewSavePhotoBtns;
@property (weak, nonatomic) IBOutlet UIView *viewTabContainer;

@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showTabBarControls:) name:@"SHOW_HIDE_TABBAR" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideCameraControBar:) name:@"HIDE_TABBAR" object:nil];
    
    [_btnProjSum setSelected:YES];
    [_imgViewHighlightProj setHighlighted:YES];
    [self showTabBarNavigation:NO];

    //project summery
    ProjectSummaryVC *pSummary = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectSummary"];
    //calendar
    CalendarVC *calendarVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Calendar"];

    self.navProjectSummary = [[UINavigationController alloc] initWithRootViewController:pSummary];
    self.navProjectSummary.toolbarHidden = YES;
    self.navProjectSummary.navigationBar.hidden = YES;
    
    //create tabbar
    self.tabbarVC = [[UITabBarController alloc] init];
    self.tabbarVC.tabBar.hidden = YES;
    self.tabbarVC.viewControllers = @[self.navProjectSummary, calendarVC];
    [self.viewTabContainer addSubview:self.tabbarVC.view];
    
    self.tabbarVC.view.frame = CGRectMake(0, 0,
                                          [GlobalShareInstance getDeviceScreen].width,
                                          [GlobalShareInstance getDeviceScreen].height);
    self.tabbarVC.selectedIndex = 0;
    [self.view sendSubviewToBack:self.tabbarVC.view];
    [self.tabbarVC.view sendSubviewToBack:self.viewTabBar];
    
    //add swipe
    self.navProjectSummary.interactivePopGestureRecognizer.delegate = self;
    self.navProjectSummary.delegate = self;
    self.viewSavePhotoBtns.hidden = YES;
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    UIViewController *vc = [navigationController.viewControllers lastObject];

    if ([vc isKindOfClass:[ProjectSummaryVC class]]) {
        [self showTabBarNavigation:NO];
    }else if ( [vc isKindOfClass:[UploadPhotoVC class]]) {
        
        //hide camera controls
        self.viewTabBar.alpha = 0;
        self.viewTabBar.transform = CGAffineTransformMakeTranslation(0, [[UIScreen mainScreen] bounds].size.height);
        self.viewSavePhotoBtns.hidden = NO;
    }
    else {
        [self showTabBarNavigation:YES];
    }
}

- (void)hideCameraControBar:(NSNotification*)isShown {
    [self showTabBarNavigation:![isShown.object boolValue]];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear===== %@", NSStringFromCGRect(self.tabbarVC.view.frame));

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"viewDidAppear===== %@", NSStringFromCGRect(self.tabbarVC.view.frame));
    
}

- (void)showTabBarControls:(NSNotification *)notif {
    [self showTabBarNavigation:[notif.object boolValue] ];
}

- (void)showTabBarNavigation:(BOOL)isShow {
    if (isShow) {
        
        self.viewSavePhotoBtns.hidden = YES;

        [UIView animateWithDuration:0.3f animations:^{
            self.viewTabBar.alpha = 1;
            self.viewTabBar.transform = CGAffineTransformIdentity;
        }];
    }else {
        [UIView animateWithDuration:0.3f animations:^{
            self.viewTabBar.alpha = 0;
            self.viewTabBar.transform = CGAffineTransformMakeTranslation(0, [[UIScreen mainScreen] bounds].size.height);
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btnTopNav_TouchUpInside:(UIButton *)sender {
    //already selected
    if (sender.selected) {
        return;
    }
    self.btnProjSum.selected = NO;
    self.btnCalendar.selected = NO;
    sender.selected = YES;
    self.tabbarVC.selectedIndex = sender.tag;
    
    if (sender == self.btnProjSum) {
        [_imgViewHighlightProj setHighlighted:YES];
        [_imgViewHighlightCalc setHighlighted:NO];
    }else {
        [_imgViewHighlightProj setHighlighted:NO];
        [_imgViewHighlightCalc setHighlighted:YES];
        [self showTabBarNavigation:NO];
        self.viewSavePhotoBtns.hidden = YES;

    }
}

#pragma mark - Actions
- (IBAction)takePhotoButton_TouchUpInside:(id)sender
{
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)) {
        [self loadUploadPhotoController];
    }else {
        UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
        mediaUI.sourceType = UIImagePickerControllerSourceTypeCamera;
        mediaUI.allowsEditing = NO;
        mediaUI.delegate = self;
        [self presentViewController:mediaUI animated:YES completion:nil];
    }
}

#pragma mark - image picker delegate
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self showTabBarNavigation:NO];
    [self.photoVC removeFromParentViewController];
    UIImage *image = (UIImage *) [info objectForKey:
                                  UIImagePickerControllerOriginalImage];
    self.photoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UploadPhoto"];
    [self.photoVC setDelegate:self];
    self.photoVC.image = image;
    [picker dismissViewControllerAnimated:NO completion:^{
        // Do something with the image
        [self.navProjectSummary pushViewController:self.photoVC animated:YES];
    }];
}

- (void)loadUploadPhotoController {
    [self.photoVC removeFromParentViewController];
    self.photoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UploadPhoto"];
    self.photoVC.view.frame = CGRectMake(0, 0, [GlobalShareInstance getDeviceScreen].width, [GlobalShareInstance getDeviceScreen].height);
    
    self.photoVC.image = [UIImage imageNamed:@"IMG_3435.JPG"];
    [self.navProjectSummary pushViewController:self.photoVC animated:YES];
    [self showTabBarNavigation:NO];
}

- (IBAction)btnPhotoLibrary_TouchUpInside:(id)sender {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *picker=[[UIImagePickerController alloc] init];
        picker.delegate=self;
        picker.allowsEditing=YES;
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = @[(NSString *) kUTTypeImage];
        [self presentViewController:picker
                           animated:YES completion:nil];
    }else
    {
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Error Accessing Photo Library" message:@"Device does not support a photo library" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alert show];
    }
 
}

//////////////////////////////////////
#pragma mark - UIImagePickerController Delegate
//////////////////////////////////////
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - upload photo controller nav
- (IBAction)btnCancel_TouchUpInside:(id)sender {
    [self.photoVC btnCancel_TouchUpInside:sender];
}

- (IBAction)btnSave_TouchUpInside:(id)sender {
    [self.photoVC btnSave_TouchUpInside:sender];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if([navigationController isKindOfClass:[UIImagePickerController class]])
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

@end
