
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Global.h
//
//  Created by nayminsoe on 6/3/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifdef DEBUG
#   define DLog(fmt, ...) {NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);}
#   define ELog(err) {if(err) DLog(@"%@", err)}
#else
#   define DLog(...)
#   define ELog(err)
#endif

// ALog always displays output regardless of the DEBUG setting
#define ALog(fmt, ...) {NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);};

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]

#define GlobalShareInstance [Global sharedInstance]


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@interface Global : NSObject

@property (nonatomic, strong) id selectedProject;

+ (Global*)sharedInstance;

- (NSString *)showAppVersion;
- (NSString *)showAppVersionBuildNumber;
- (NSString *)getTimeStampDateFormat:(NSDateFormatterStyle)dateStyle timeFormat:(NSDateFormatterStyle)timeStyle;
- (NSString *)encodeToPercentEscapeString:(NSString *)string;
- (NSString *)decodeFromPercentEscapeString:(NSString *)string;
- (BOOL)validateEmail:(NSString *)candidate;
- (NSArray *)loadPlistFile:(NSString *)filename;
- (NSDictionary *)loadPlistFileDictionary:(NSString *)filename;

- (NSArray *)loadArrayPlistFile:(NSString *)filename;
- (NSString *)getPlistPath:(NSString *)filename;
- (NSString *)cachedPathForFileName:(NSString *)name;
- (NSString *)htmlEntityDecode:(NSString *)string;
+ (NSString *)convertDateToString:(NSDate *)yourDate wantedFormat:(NSString *)format;
- (NSDate*)convertStringToDate:(NSString*)dateString;
- (NSString *) convertToCelsius:(float) val;
- (NSString *) convertToFahrenheit:(float) val;
+ (NSString *)formatString:(double)total withGroupSize:(int)size;
 
- (NSString *)stringByStrippingHTML:(NSString *)inputString;
- (UIImage *)resizeImageToSize:(CGSize)targetSize : (UIImage*) sourceImg;
- (BOOL)isLowerThaniOS_7;
- (NSDate *)dateFormat:(NSString*)date;
- (UIColor *)getColor:(NSString *)percent;
/*!
 \internal
 @function isNetworkReachable.
 @abstract Check the internet reachabilty.
 @discussion
 @param
 @result isReachable YES if network is available else NO.
 */
+ (BOOL) isNetworkReachable;

+(CGRect )getTheFrameAsPerTheOrientation;
- (BOOL)checkEmptyEntry:(NSString *)input;
- (CGSize)getDeviceScreen;
@end
