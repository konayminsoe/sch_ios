//
//  SwipeDownSegue.m
//  CustomSegue
//
//  Created by Nay Min Soe on 25/11/14.
//  Copyright (c) 2014 Possible. All rights reserved.
//

#import "SwipeDownSegue.h"

@implementation SwipeDownSegue

- (void)perform {
    UIViewController *srcViewController = (UIViewController *) self.sourceViewController;
    UIViewController *destViewController = (UIViewController *) self.destinationViewController;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    [srcViewController.view.window.layer addAnimation:transition forKey:nil];
    
    [srcViewController presentViewController:destViewController animated:NO completion:nil];
}

@end
