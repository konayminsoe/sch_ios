//
//  Global.m
//
//  Created by nayminsoe on 6/3/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "Global.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#import "Reachability.h"

@interface Global()
{
}

@end


@implementation Global

+ (Global*)sharedInstance
{
    static Global *sharedInstance; 
    static dispatch_once_t done; 
    dispatch_once(&done, ^{
        sharedInstance = [[Global alloc] init]; });

    return sharedInstance; 
}

- (id)init
{
    if ((self = [super init])) {

    }
    return self;
}

// -------------------------------------------------------------------------
// Shows Alertview
// -------------------------------------------------------------------------
- (void) showAlert:(NSString *)title message:(NSString *)message
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title 
                                                    message:message 
                                                   delegate:nil 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
	[alert show];
}

// -------------------------------------------------------------------------
// Show the App Version
// -------------------------------------------------------------------------
- (NSString *) showAppVersion
{
	NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
	NSString *version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
	NSString *sReturn = [NSString stringWithFormat:@"%@", version];
	
	return sReturn;
}

// -------------------------------------------------------------------------
// Show the App Version w/ Build Number
// -------------------------------------------------------------------------
- (NSString *)showAppVersionBuildNumber
{
	NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
	NSString *version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *revision = [infoDictionary objectForKey:@"CFBundleVersion"];

	NSString *sReturn = [NSString stringWithFormat:@"Version %@.%@", version, revision];
	
	return sReturn;
}


// -------------------------------------------------------------------------
// Get the Current Date & Time
// Timestamp format: YYYY-MM-DD HH:MM:SS
// -------------------------------------------------------------------------
- (NSString *)getTimeStampDateFormat:(NSDateFormatterStyle)dateStyle timeFormat:(NSDateFormatterStyle)timeStyle
{
    NSString *dateInString = nil;
    // Get current datetime
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    if (dateStyle == NSDateFormatterNoStyle || timeStyle == NSDateFormatterNoStyle) {        
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    }
    else {
        [dateFormatter setDateStyle:dateStyle];
        [dateFormatter setTimeStyle:timeStyle];
    }
    dateInString = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInString;
}

// -------------------------------------------------------------------------
// Encode a string to embed in an URL.
// -------------------------------------------------------------------------
- (NSString *) encodeToPercentEscapeString:(NSString *)string 
{    
    return CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                     (__bridge CFStringRef) string,
                                                                     NULL,
                                                                     (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                                     kCFStringEncodingUTF8));
}

// -------------------------------------------------------------------------
// Decode a percent escape encoded string.
// -------------------------------------------------------------------------
- (NSString *) decodeFromPercentEscapeString:(NSString *)string 
{    
    return CFBridgingRelease( CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, 
                                                                                      (__bridge CFStringRef) string,
                                                                                      CFSTR(""), 
                                                                                      kCFStringEncodingUTF8) );
}

// -------------------------------------------------------------------------
// Desc	 : Validate email formatting
// Return: True if email format is good, else email is bad
// -------------------------------------------------------------------------
- (BOOL)validateEmail:(NSString *)candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
	
    return [emailTest evaluateWithObject:candidate];
}

// -------------------------------------------------------------------------
// Load Plist File
// -------------------------------------------------------------------------
- (NSArray *)loadPlistFile:(NSString *)filename
{    
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    NSArray *aList = [dict objectForKey:@"Root"];
    
    return aList;
}

- (NSDictionary *)loadPlistFileDictionary:(NSString *)filename
{
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    return dict;
}

// -------------------------------------------------------------------------
// Load Array Plist File
// -------------------------------------------------------------------------
- (NSArray *)loadArrayPlistFile:(NSString *)filename
{
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"plist"];
    NSArray *aList = [[NSArray alloc] initWithContentsOfFile:path];
    
    return aList;
}

// -------------------------------------------------------------------------
// Retrieves the plist path
// -------------------------------------------------------------------------
- (NSString *)getPlistPath:(NSString *)filename 
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    NSString *path = [cachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",filename]];

    return path;
}

// -------------------------------------------------------------------------
// Get cached filepath
// -------------------------------------------------------------------------
- (NSString *)cachedPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    
    return [cachePath stringByAppendingPathComponent:name];
}



-(NSString *)htmlEntityDecode:(NSString *)string
{
    ////DLog(@"before >>> - %@",string);
    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    ////DLog(@"after >>> - %@",string);
    return string;

}


NSDateFormatter *_dateFormatter = nil;

#pragma mark - convert string from Date
+ (NSString *)convertDateToString:(NSDate *)yourDate wantedFormat:(NSString *)format
{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
     }
    [_dateFormatter setDateFormat:format];
    return [_dateFormatter stringFromDate:yourDate];
}

//Mon Apr 08 14:18:20 SGT 2013
//dateCreated = "2014-03-28T10:04:47Z";
//hh:mm LLL dd | EEE
- (NSDate*)convertStringToDate:(NSString*)dateString
{
    if (!_dateFormatter) {
        _dateFormatter  =   [[NSDateFormatter alloc]init];
    }
    [_dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    return [_dateFormatter dateFromString:dateString];
}
 
-(NSString *) convertToCelsius:(float) val
{
    float result=(val-32) * 5/9;
    return [NSString stringWithFormat:@"%.0f",result];
}

-(NSString *) convertToFahrenheit:(float) val
{
    float result=(val*9)/5+32;
    return [NSString stringWithFormat:@"%.0f",result];
}

- (NSString *)stringByStrippingHTML:(NSString *)inputString
{
    NSMutableString *outString;
    
    if (inputString)
    {
        outString = [[NSMutableString alloc] initWithString:inputString];
        
        if ([inputString length] > 0)
        {
            NSRange r;
            
            while ((r = [outString rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
            {
                [outString deleteCharactersInRange:r];
            }
        }
    }
    return outString;
}


#pragma mark - group number with ','
static NSNumberFormatter *numberFormater;
+ (NSString *)formatString:(double)total withGroupSize:(int)size
{
    if (!numberFormater) {
        numberFormater = [[NSNumberFormatter alloc]init];
        [numberFormater setNumberStyle: NSNumberFormatterDecimalStyle];
    }
    [numberFormater setGroupingSize:size];
    [numberFormater setGroupingSeparator:@","];
    [numberFormater setUsesGroupingSeparator: TRUE];
    
    return [numberFormater stringFromNumber:[NSNumber numberWithDouble:total]];
}



- (UIImage *)resizeImageToSize:(CGSize)targetSize : (UIImage*) sourceImg
{
    UIImage *sourceImage = sourceImg; //self.imageToFitInBackground;
    UIImage *newImage = nil;
    
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    
    //CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        /*
        if (widthFactor < heightFactor)
            scaleFactor = widthFactor;
        else
            scaleFactor = heightFactor;
        */
        //scaledWidth  = width * scaleFactor;
        //scaledHeight = height * scaleFactor;
        scaledWidth  = targetWidth;
        scaledHeight = targetHeight;

        
        // make image center aligned
        if (widthFactor < heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor > heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(targetSize);
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
     
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if(newImage == nil)
        return nil;
    
    return sourceImg ;
}

  
- (BOOL)isLowerThaniOS_7 {
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
        return YES;
    }
    return NO;
}


/*!
 @function isNetworkReachable.
 @abstract Check internet reachabilty.
 @discussion
 @param
 @result return YES if network is available.
 */
+ (BOOL) isNetworkReachable {
    
    BOOL isReachable = YES;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus reachabilityStatus = [reachability currentReachabilityStatus];
    
    if (reachabilityStatus == NotReachable)
        isReachable= NO;
    
    return isReachable;
    
}

+(CGRect )getTheFrameAsPerTheOrientation {
    
    CGRect rect;
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight){
        
        rect= CGRectMake(0, 0, 1024, 768);
    }
    else {
        
        rect= CGRectMake(0, 0, 768, 1024);
    }
    
    return rect;
}

- (NSDate *)dateFormat:(NSString*)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    return [formatter dateFromString:date];
}

- (UIColor *)getColor:(NSString *)percent
{
    float perc = [percent floatValue];
    if (perc <= 25) {
        return RGB(255, 0, 0);
    }else if (perc < 100) {
        return RGB(255, 206, 0);
    }else
        return RGB(53, 222, 57);
}

- (BOOL)checkEmptyEntry:(NSString *)input {
    return [input
            stringByReplacingOccurrencesOfString:@" " withString:@""].length > 0 ? NO : YES;
}

- (CGSize)getDeviceScreen {
    return [UIScreen mainScreen].bounds.size;
}
@end
