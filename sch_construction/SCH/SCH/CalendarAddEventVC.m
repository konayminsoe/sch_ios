//
//  CalendarAddEventVC.m
//  SCH
//
//  Created by nayminsoe on 1/12/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import "CalendarAddEventVC.h"
#import "Global.h"

@interface CalendarAddEventVC ()<UITextFieldDelegate>
@property (nonatomic, weak) UITextField *activeTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *dateTimePicker;
@property (weak, nonatomic) IBOutlet UIView *viewPicker;

@property (weak, nonatomic) IBOutlet UITextField *txtfProject;
@property (weak, nonatomic) IBOutlet UITextField *txtfVenue;
@property (weak, nonatomic) IBOutlet UIButton *btnDay;
@property (weak, nonatomic) IBOutlet UIButton *btnMonth;
@property (weak, nonatomic) IBOutlet UIButton *btnYear;
@property (weak, nonatomic) IBOutlet UITextField *txtfHours;

@end

@implementation CalendarAddEventVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewPicker.transform = CGAffineTransformMakeTranslation(0, 300);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // Step 3: Scroll the target text field into view.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(aRect, self.activeTextField.frame.origin) ) {
        
        //self.activeTextField.frame.origin.y - (keyboardSize.height)
        
        self.view.window.layer.transform = CATransform3DMakeTranslation(0, -keyboardSize.height, 0);
        
        //self.view.transform = CGAffineTransformMakeTranslation(0, -keyboardSize.height);
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    self.view.window.layer.transform = CATransform3DIdentity;
    //self.view.transform = CGAffineTransformIdentity;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation
- (IBAction)btnAddEvent_TouchUpInside:(id)sender {
    
    
    if ([_btnDay.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]].length == 0
        ||
        [_txtfProject.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]].length == 0
        ||
        [_txtfVenue.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]].length == 0
        ||
        [_txtfHours.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]].length == 0) {
        
        [[[UIAlertView alloc] initWithTitle:@"Empty Fields!" message:@"All fields are compulsory to be filled." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        
        return;
    }
    
    NSString *date = [Global convertDateToString:self.dateTimePicker.date wantedFormat:@"yyyy-MM-dd hh:mm:ssZ"];
    date = [date stringByReplacingOccurrencesOfString:@" " withString:@"T"];
    
    NSDictionary *newEvent = [NSDictionary dictionaryWithObjectsAndKeys:
                        date, @"date",
                        self.txtfHours.text, @"hours",
                        self.txtfProject.text, @"project",
                        self.txtfVenue.text, @"venue",nil];
    
    [self.delegate didAddEvent:newEvent];
    [self.activeTextField resignFirstResponder];
    [self dismissAddEventView];
}

- (IBAction)btnCancel_TouchUpInside:(id)sender {
    [self.activeTextField resignFirstResponder];
    [self dismissAddEventView];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)dismissAddEventView {
    [UIView animateWithDuration:0.3f animations:^{
        self.view.frame = CGRectMake(0,
                                     [UIScreen mainScreen].bounds.size.height,
                                     self.view.frame.size.width,  self.view.frame.size.height);
    }];
}
- (IBAction)btnDateTime_TouchUpInside:(UIButton *)sender {
    
    [UIView animateWithDuration:0.3f animations:^{
        self.viewPicker.transform = CGAffineTransformIdentity ;
        self.dateTimePicker.date = [NSDate date];
    }];
}

- (IBAction)btnChooseDateDone_TouchUpInside:(id)sender {
    [UIView animateWithDuration:0.3f animations:^{
        self.viewPicker.transform = CGAffineTransformMakeTranslation(0, 300);
    }];
    
    [self.btnDay setTitle:[Global convertDateToString:self.dateTimePicker.date wantedFormat:@"dd"] forState:UIControlStateNormal];
    
    [self.btnMonth setTitle:[Global convertDateToString:self.dateTimePicker.date wantedFormat:@"MM"] forState:UIControlStateNormal];

    
    [self.btnYear setTitle:[Global convertDateToString:self.dateTimePicker.date wantedFormat:@"yyyy"] forState:UIControlStateNormal];

}

@end
