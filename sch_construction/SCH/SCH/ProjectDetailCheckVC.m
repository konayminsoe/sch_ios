//
//  ProjectDetailCheckVC.m
//  SCH
//
//  Created by nayminsoe on 11/24/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import "ProjectDetailCheckVC.h"

#define system1_max 20
#define system2_max 20
#define system3_max 13
#define system4_max 20
#define system5_max 10

@interface ProjectDetailCheckVC ()
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSystem1, *imgViewSystem2, *imgViewSystem3, *imgViewSystem4, *imgViewSystem5;

@property (weak, nonatomic) IBOutlet UILabel *lblSystem1, *lblSystem2, *lblSystem3, *lblSystem4, *lblSystem5;
@end

@implementation ProjectDetailCheckVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btnSystem1_TouchUpInside:(UIButton *)sender {
    
    int job_done = _lblSystem1.text.intValue;
    switch (sender.tag) {
        case 0:
            if (job_done > 0) {
                job_done--;
            }
            break;
        default:
            if (job_done < system1_max) {
                job_done++;
            }
            break;
    }
    self.lblSystem1.text = [NSString stringWithFormat:@"%0d", job_done];
}

- (IBAction)btnSystem2_TouchUpInside:(UIButton *)sender {
    int job_done = _lblSystem2.text.intValue;
    switch (sender.tag) {
        case 0:
            if (job_done > 0) {
                job_done--;
            }
            break;
        default:
            if (job_done < system2_max) {
                job_done++;
            }
            break;
    }
    self.lblSystem2.text = [NSString stringWithFormat:@"%0d", job_done];
}

- (IBAction)btnSystem3_TouchUpInside:(UIButton *)sender {
    int job_done = _lblSystem3.text.intValue;
    switch (sender.tag) {
        case 0:
            if (job_done > 0) {
                job_done--;
            }
            break;
        default:
            if (job_done < system3_max) {
                job_done++;
            }
            break;
    }
    self.lblSystem3.text = [NSString stringWithFormat:@"%0d", job_done];
}

- (IBAction)btnSystem4_TouchUpInside:(UIButton *)sender {
    int job_done = _lblSystem4.text.intValue;
    switch (sender.tag) {
        case 0:
            if (job_done > 0) {
                job_done--;
            }
            break;
        default:
            if (job_done < system4_max) {
                job_done++;
            }
            break;
    }
    self.lblSystem4.text = [NSString stringWithFormat:@"%0d", job_done];
}

- (IBAction)btnSystem5_TouchUpInside:(UIButton *)sender {
    int job_done = _lblSystem5.text.intValue;
    switch (sender.tag) {
        case 0:
            if (job_done > 0) {
                job_done--;
            }
            break;
        default:
            if (job_done < system5_max) {
                job_done++;
            }
            break;
    }
    self.lblSystem5.text = [NSString stringWithFormat:@"%0d", job_done];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //TODO
    //post latest jobdone to server
}
@end
