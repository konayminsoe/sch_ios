//
//  CollectionHeaderView.h
//  SCH
//
//  Created by nayminsoe on 11/25/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionHeaderView : UICollectionReusableView

@property(nonatomic, weak) IBOutlet UILabel *lblSectionHeader;

@end
