//
//  ProjectCell.h
//  SCH
//
//  Created by nayminsoe on 11/25/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DACircularProgressView.h"

@interface ProjectCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblProjectName;
@property (nonatomic, weak) IBOutlet UILabel *lblNextTestDate;
@property (nonatomic, weak) IBOutlet UILabel *lblPrice;
@property (nonatomic, weak) IBOutlet UILabel *lblPercent;
@property (strong, nonatomic) IBOutlet DACircularProgressView *progress_view;

- (void)bind:(id)project;
@end
