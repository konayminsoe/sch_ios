//
//  ProjectDetailCell.h
//  SCH
//
//  Created by nayminsoe on 1/18/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DACircularProgressView.h"

@interface ProjectDetailCell : UITableViewCell

@property (strong, nonatomic) IBOutlet DACircularProgressView *progress_view;
@property (nonatomic, weak) IBOutlet UILabel *lblProjectName;
@property (nonatomic, weak) IBOutlet UILabel *lblAddress;
@property (nonatomic, weak) IBOutlet UILabel *lblPercent;

- (void)bind:(id)project;

@end
