//
//  ProjectCell.m
//  SCH
//
//  Created by nayminsoe on 11/25/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import "ProjectCell.h"
#import "Global.h"

@implementation ProjectCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)bind:(id)project {
    
    NSDate *date = [[Global sharedInstance] dateFormat:project[@"testdate"]];
    _lblProjectName.text = project[@"projectname"];
    _lblNextTestDate.text = [NSString stringWithFormat:@"Next test date : %@", [Global convertDateToString:date wantedFormat:@"dd/MM/yy"]];
    _lblPrice.text = project[@"price"];
    _lblPercent.text = [NSString stringWithFormat:@"%@%%", project[@"percent"]];
    [self progress:project[@"percent"]];
}

- (void)progress:(NSString *)percent {
    self.progress_view.roundedCorners = YES;
    self.progress_view.thicknessRatio = 0.2;
    self.progress_view.trackTintColor = [UIColor whiteColor];
    self.progress_view.progressTintColor = [GlobalShareInstance getColor:percent];
    [self.progress_view setProgress:[percent floatValue]/100.0 animated:YES];
}
@end
