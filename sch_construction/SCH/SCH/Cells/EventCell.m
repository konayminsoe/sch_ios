//
//  EventCell.m
//  SCH
//
//  Created by nayminsoe on 1/15/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import "EventCell.h"
#import "Global.h"

@implementation EventCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)bind:(id)data {
    
    NSDate *date = [GlobalShareInstance dateFormat:data[@"date"]];
    [_lblDatetime setText:[Global convertDateToString:date wantedFormat:@"dd/MM/yy,"]];
    [_lblProjHours setText:data[@"hours"]];
    [_lblProject setText:data[@"project"]];
    [_lblAddress setText:data[@"venue"]];
}
@end
