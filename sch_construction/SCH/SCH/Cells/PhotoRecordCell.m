//
//  PhotoRecordCell.m
//  SCH
//
//  Created by nayminsoe on 1/18/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import "PhotoRecordCell.h"
#import "Global.h"

@implementation PhotoRecordCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)bind:(id)info {
    NSDate *date = [GlobalShareInstance dateFormat:info[@"date"]];
    _lblPhotoTitle.text = [NSString stringWithFormat:@"%@ %@HR",
                           [Global convertDateToString:date wantedFormat:@"dd MMMM yyyy"], info[@"hours"]];
    _lblPhotoDesc.text = info[@"desc"];
}
@end
