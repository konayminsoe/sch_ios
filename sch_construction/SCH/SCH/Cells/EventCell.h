//
//  EventCell.h
//  SCH
//
//  Created by nayminsoe on 1/15/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblProjHours;
@property (weak, nonatomic) IBOutlet UILabel *lblDatetime;
@property (weak, nonatomic) IBOutlet UILabel *lblProject;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;

- (void)bind:(id)data;
@end
