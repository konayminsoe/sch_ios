//
//  ProjectDetailCell.m
//  SCH
//
//  Created by nayminsoe on 1/18/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import "ProjectDetailCell.h"
#import "Global.h"

@implementation ProjectDetailCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)bind:(id)project {
    
    _lblProjectName.text = project[@"projectname"];
    _lblAddress.text = project[@"address"];
    _lblPercent.text = [NSString stringWithFormat:@"%@%%", project[@"percent"]];
    [self progress:project[@"percent"]];
}

- (void)progress:(NSString *)percent {
    self.progress_view.roundedCorners = YES;
    self.progress_view.thicknessRatio = 0.2;
    self.progress_view.trackTintColor = [UIColor whiteColor];
    self.progress_view.progressTintColor = [GlobalShareInstance getColor:percent];
    [self.progress_view setProgress:[percent floatValue]/100.0 animated:YES];
}
@end
