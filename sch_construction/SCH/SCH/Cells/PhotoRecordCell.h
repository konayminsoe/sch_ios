//
//  PhotoRecordCell.h
//  SCH
//
//  Created by nayminsoe on 1/18/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoRecordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblPhotoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPhotoDesc;

- (void)bind:(id)info;
@end
