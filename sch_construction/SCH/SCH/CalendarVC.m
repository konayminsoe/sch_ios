//
//  CalendarVC.m
//  SCH
//
//  Created by nayminsoe on 11/24/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import "CalendarVC.h"
#import "CalendarAddEventVC.h"
#import "DPCalendarMonthlySingleMonthViewLayout.h"
#import "DPCalendarMonthlyView.h"
#import "DPCalendarEvent.h"
#import "DPCalendarIconEvent.h"
#import "NSDate+DP.h"
#import "Global.h"
#import "EventCell.h"


@interface CalendarVC ()<CalendarAddEventDelegate, DPCalendarMonthlyViewDelegate, UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *eventDates;
    NSMutableDictionary *events;
    NSDate *currentMonth;
}
@property (nonatomic, strong) CalendarAddEventVC *addEventVC;

@property (nonatomic, strong) UILabel *monthLabel;
@property (nonatomic, strong) UIButton *previousButton;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic, strong) UIButton *createEventButton;
@property (nonatomic, strong) DPCalendarMonthlyView *monthlyView;
@property (nonatomic, strong) IBOutlet UITableView *tblEvents;


@end

@implementation CalendarVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self commonInit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btnAddEvent_TouchUpInside:(id)sender {
    _addEventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CalendarAddEventVC"];
    [_addEventVC setDelegate:self];
    
    self.addEventVC.view.frame = CGRectMake(0,  self.view.frame.size.height, [UIScreen mainScreen].bounds.size.width,  240);
    
    [self.view addSubview:_addEventVC.view];
    
    float y = [UIScreen mainScreen].bounds.size.height - (143 + self.addEventVC.view.frame.size.height);
    
    [UIView animateWithDuration:0.3f animations:^{
        self.addEventVC.view.frame = CGRectMake(0, y,
                                                [UIScreen mainScreen].bounds.size.width,  240);
    }];
}
#pragma mark - delegate
- (void)didAddEvent:(id)newEvent {
    
    //TODO
    //save this new event to api
    [self initEventData];
    
    //add new event
    [eventDates addObject:newEvent];
    
    //update
    [self updateData];

}

-(void) commonInit {
    events = [NSMutableDictionary dictionary];
    [self initEventData];
    [self generateMonthlyView];
    [self updateLabelWithMonth:self.monthlyView.seletedMonth];

}

- (void)initEventData {
    NSDictionary *d1 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-01-14T11:24:00Z", @"date",
                        @"1000", @"hours",
                        @"HDB Housing", @"project",
                        @"Blk-718, #02-213, Yishun St71", @"venue",nil];
    
    NSDictionary *d2 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-01-16T11:24:00Z", @"date",
                        @"1000", @"hours",
                        @"Over Bridge", @"project",
                        @"Blk-718, #02-213, Yishun St71", @"venue",nil];
    
    NSDictionary *d3 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-01-18T11:24:00Z", @"date",
                        @"1000", @"hours",
                        @"School", @"project",
                        @"Blk-718, #02-213, Yishun St71", @"venue",nil];
    
    NSDictionary *d4 = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"2015-02-01T01:24:00Z", @"date",
                        @"1000", @"hours",
                        @"HDB", @"project",
                        @"Blk-718, #02-213, Yishun St71", @"venue",nil];
    
    eventDates = [NSMutableArray array];
    [eventDates addObject:d1];
    [eventDates addObject:d2];
    [eventDates addObject:d3];
    [eventDates addObject:d4];

}

- (void)filterEventsBasedonMonth {
    
    NSArray *tempEvents = [NSArray arrayWithArray:eventDates];
    [eventDates removeAllObjects];
    
    [tempEvents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSDate *date = [GlobalShareInstance dateFormat:obj[@"date"]];
        NSString *month = [Global convertDateToString:date wantedFormat:@"MMMM"];
        
        NSString *activeMonth = [Global convertDateToString:currentMonth wantedFormat:@"MMMM"];
        
        NSLog(@"month = %@, %@", month, activeMonth);
        
        if ([month isEqualToString: activeMonth]) {
            //add
            [eventDates addObject:obj];
        }
    }];
  
}

- (void) generateMonthlyView {
    CGFloat height = self.view.bounds.size.width;
    CGFloat width = 33*6;
    
    [self.previousButton removeFromSuperview];
    [self.nextButton removeFromSuperview];
    [self.monthLabel removeFromSuperview];
    
    self.previousButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.nextButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    self.previousButton.frame = CGRectMake(50, 0, 50, 44);
    self.nextButton.frame = CGRectMake(height - 100, 0, 50, 44);
    
    [self.previousButton setTitle:@"<" forState:UIControlStateNormal];
    [self.nextButton setTitle:@">" forState:UIControlStateNormal];
    [self.previousButton setTintColor:[UIColor orangeColor]];
    [self.nextButton setTintColor:[UIColor orangeColor]];

    self.monthLabel = [[UILabel alloc] initWithFrame:CGRectMake((height - 200) / 2, 0, 200, 44)];
    [self.monthLabel setTextAlignment:NSTextAlignmentCenter];
    self.monthLabel.textColor = [UIColor orangeColor];
    
    [self.previousButton addTarget:self action:@selector(previousButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self.nextButton addTarget:self action:@selector(nextButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
 
    [self.monthlyView removeFromSuperview];
    self.monthlyView = [[DPCalendarMonthlyView alloc] initWithFrame:CGRectMake(0, 44, height, width) delegate:self];
    [self.monthlyView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:self.monthlyView];
    [self.view addSubview:self.monthLabel];
    [self.view addSubview:self.previousButton];
    [self.view addSubview:self.nextButton];
    [self.view addSubview:self.createEventButton];
}

- (void) updateData {
    NSMutableArray *allevents = @[].mutableCopy;
    
    NSDate *date = [[NSDate date] dateByAddingYears:0 months:0 days:0];

    for (int i = 0; i < eventDates.count; i++) {
        int index = i;
        id dict = eventDates[i];
        date = [GlobalShareInstance dateFormat:dict[@"date"]];
        DPCalendarEvent *event = [[DPCalendarEvent alloc] initWithTitle:nil startTime:date endTime:[date dateByAddingYears:0 months:0 days:0] colorIndex:index];
        [allevents addObject:event];
    }
    
    [self.monthlyView setEvents:allevents complete:nil];
    [self filterEventsBasedonMonth];
    [self.tblEvents reloadData];
}

-(void) previousButtonSelected:(id)button {
    [self.monthlyView scrollToPreviousMonthWithComplete:nil];
}

-(void) nextButtonSelected:(id)button {
    [self.monthlyView scrollToNextMonthWithComplete:nil];
}

-(void) todayButtonSelected:(id)button {
    [self.monthlyView clickDate:[NSDate date]];
}

- (void) createEventButtonSelected:(id)button {
    UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-160, 320, 160)];
    [picker setDate:[NSDate date]];
    [self.view addSubview:picker];
}

- (void) updateLabelWithMonth:(NSDate *)month {
    currentMonth = month;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM YYYY"];
    NSString *stringFromDate = [formatter stringFromDate:month];
    [self.monthLabel setText:[stringFromDate uppercaseString]];
    
    //TODO
    [self initEventData];
    [self updateData];

}

#pragma DPCalendarMonthlyViewDelegate
-(void)didScrollToMonth:(NSDate *)month firstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate{
    [self updateLabelWithMonth:month];
}

-(BOOL)shouldHighlightItemWithDate:(NSDate *)date {
    return YES;
}

-(BOOL)shouldSelectItemWithDate:(NSDate *)date {
    return YES;
}

NSArray *source = nil;
-(void)didSelectItemWithDate:(id)selected_events :(UICollectionViewCell *)cell {
    
    if ([selected_events count] == 0) {
        return;
    }
    source = selected_events;
 }

-(NSDictionary *) iphoneMonthlyViewAttributes {
    return @{
             DPCalendarMonthlyViewAttributeEventDrawingStyle: [NSNumber numberWithInt:DPCalendarMonthlyViewEventDrawingStyleUnderline],
             DPCalendarMonthlyViewAttributeCellNotInSameMonthSelectable: @YES,
             DPCalendarMonthlyViewAttributeMonthRows:@3
             };
    
}

-(BOOL)shouldAutorotate {
    return NO;
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self commonInit];
}

-(NSDictionary *)monthlyViewAttributes {
    return [self iphoneMonthlyViewAttributes];
}

#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 68.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return eventDates.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"EventCell";
    EventCell *cell = (EventCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setBackgroundColor:[UIColor clearColor]];
    [tableView setBackgroundColor:[UIColor clearColor]];
    [tableView setSeparatorColor:[UIColor darkTextColor]];
    [cell bind:eventDates[indexPath.row]];
    return cell;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
@end
