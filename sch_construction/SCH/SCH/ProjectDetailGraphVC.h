//
//  ProjectDetailGraphVC.h
//  SCH
//
//  Created by nayminsoe on 11/24/14.
//  Copyright (c) 2014 nayminsoe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "BarGraphClass.h"


@interface ProjectDetailGraphVC : UIViewController
{
    CPTGraphHostingView *_graphHostingView;
    BarGraphClass *_BarPlot;
}
@property (nonatomic, retain) IBOutlet CPTGraphHostingView *graphHostingView;
@property (nonatomic, retain) BarGraphClass *BarPlot;

@property (weak, nonatomic) IBOutlet UILabel *lblHours, *lblOTHours, *lblMaterials;

@property (weak, nonatomic) IBOutlet UILabel *lblHours_salary, *lblOTHours_salary, *lblMaterials_salary;

@property (weak, nonatomic) IBOutlet UILabel * lblTotalCost_salary;

@end
