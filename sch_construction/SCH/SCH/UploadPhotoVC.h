//
//  UploadPhotoVC.h
//  SCH
//
//  Created by nayminsoe on 1/22/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadPhotoVC : UIViewController
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, weak) id delegate;

- (IBAction)btnCancel_TouchUpInside:(id)sender;
- (IBAction)btnSave_TouchUpInside:(id)sender;
  
@end
