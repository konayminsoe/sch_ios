//
//  ProjectDetailPhotoPreviewVC.m
//  SCH
//
//  Created by nayminsoe on 1/18/15.
//  Copyright (c) 2015 nayminsoe. All rights reserved.
//

#import "ProjectDetailPhotoPreviewVC.h"

@interface ProjectDetailPhotoPreviewVC ()
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIView *viewDesc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vertConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@end

@implementation ProjectDetailPhotoPreviewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.imgViewPhoto addGestureRecognizer:[[UITapGestureRecognizer alloc]  initWithTarget:self action:@selector(didTapImage:)]];
    self.viewDesc.alpha = 0;
    if ([UIScreen mainScreen].bounds.size.height == 568) {
        self.vertConstraint.constant = 214;
    }else {
        self.vertConstraint.constant = 325;
    }
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    self.vertConstraint.constant = 0;
    self.viewDesc.alpha = 1;
    if ([UIScreen mainScreen].bounds.size.height == 568) {
        self.vertConstraint.constant = 214;
    }else {
        self.vertConstraint.constant = 313;
    }
    
    
}

- (void)didTapImage:(UITapGestureRecognizer *)gest {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
